package window

import (
	"time"
)

type Window interface {
	Goto(value time.Time) (start time.Time, end time.Time)
	Jump(delta int) (time.Time, time.Time)
	Items() int
}

func NewUnix(interval time.Duration, limit int) Window {
	return New(time.Unix(0, 0).UTC(), interval, limit)
}

func New(anchor time.Time, interval time.Duration, limit int) Window {
	daily := int(time.Hour * 24 / interval)
	if daily > limit {
		size := time.Hour * 24
		for {
			size /= 2
			if int(size/interval) < limit {
				break
			}
		}
		return &window{
			limit:      limit,
			interval:   interval,
			windowSize: size,
			start:      anchor,
			end:        anchor.Add(size).Add(-time.Nanosecond),
		}
	}

	dil := limit / daily
	return &dailyWindow{
		limit:       limit,
		interval:    interval,
		daysInLimit: dil,
		start:       anchor,
		end:         anchor.AddDate(0, 0, dil).Add(-time.Nanosecond),
	}
}

// sugar

func Next(value Window) (start time.Time, end time.Time) {
	return value.Jump(1)
}

func Previous(value Window) (start time.Time, end time.Time) {
	return value.Jump(-1)
}

// implementation

type dailyWindow struct {
	limit       int
	interval    time.Duration
	daysInLimit int
	start       time.Time
	end         time.Time
}

func (t *dailyWindow) Goto(value time.Time) (start time.Time, end time.Time) {
	d := int(value.Sub(t.start)/(time.Hour*24)) / t.daysInLimit
	return t.Jump(d)
}

func (t *dailyWindow) Jump(delta int) (time.Time, time.Time) {
	t.start = t.start.AddDate(0, 0, delta*t.daysInLimit)
	t.end = t.end.AddDate(0, 0, delta*t.daysInLimit)
	return t.start, t.end
}

func (t *dailyWindow) Items() int {
	return int(24 * time.Hour * time.Duration(t.daysInLimit) / t.interval)
}

// generic sub daily

type window struct {
	limit      int
	interval   time.Duration
	windowSize time.Duration
	start      time.Time
	end        time.Time
}

func (t *window) Goto(value time.Time) (start time.Time, end time.Time) {
	return t.Jump(int(value.Sub(t.start) / t.windowSize))
}

func (t *window) Jump(delta int) (time.Time, time.Time) {
	t.start = t.start.Add(time.Duration(delta) * t.windowSize)
	t.end = t.end.Add(time.Duration(delta) * t.windowSize)
	return t.start, t.end
}

func (t *window) Items() int {
	return int(t.windowSize / t.interval)
}
